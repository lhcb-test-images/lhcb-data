FROM gitlab-registry.cern.ch/lhcb-test-images/lcg92

# Now dealing with the conditions
RUN yum -y install git && yum clean all && \
    mkdir -p /opt/git-conddb && cd /opt/git-conddb && \
    git clone --bare https://gitlab.cern.ch/lhcb-conddb/DDDB.git && \
    git clone --bare https://gitlab.cern.ch/lhcb-conddb/LHCBCOND.git && \
    git clone --bare https://gitlab.cern.ch/lhcb-conddb/SIMCOND.git

# Now install missing data packages
RUN lbinstall --root=/opt/lhcb install PARAM_common \
    PARAM_ChargedProtoANNPIDParam_v1r7 \
    PARAM_ParamFiles_v8r22       \
    PARAM_QMTestFiles_v1r3       \
    DBASE_common       	       	 \
    DBASE_TCK_HltTCK_v3r19	 \
    DBASE_Gen_DecFiles_v30r8	 \
    DBASE_TCK_L0TCK_v5r27	 \
    DBASE_RawEventFormat_v1r9	 \
    DBASE_PRConfig_v1r31	 \
    DBASE_FieldMap_v5r7		 \
    DBASE_Det_SQLDDDB_v7r10	 \
    DBASE_AppConfig_v3r353	 \
    DBASE_LbEnvFix_v0r0	       && \
    rm -rf /opt/lhcb/lhcb/SQLite && \
    ln -s /opt/lhcb/lhcb/DBASE/LbEnvFix/v0r0 /opt/lhcb/lhcb/DBASE/LbEnvFix/prod

# N.B. The SQLite cond DB is large but we don't really need its payload so we remove it...